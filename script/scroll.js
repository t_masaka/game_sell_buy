
var nav_offset = $('.nav-in').offset().top;

$(window).on('scroll load',function(){
    var now_offset = $(window).scrollTop();
    if ( now_offset >= nav_offset ) {
        $('.nav-in').css('position','fixed');
    } else {
        $('.nav-in').css('position','inherit');
    }
});